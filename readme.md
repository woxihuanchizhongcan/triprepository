# The Guidebook
A social network for travellers. Share your travel experience and learn about the trips other people had.

Initially this was a graduation project 
for an online-course titled 
"Java from beginner level to creating 
your own client-server app". After the course was finished, I expanded the project to include new features.

Ports used: `8080:8080`, `5432:5432`

Required software: `Docker`

To launch the app (Method A): 

* Open command string in the project 
folder and  execute the following:
* `mvn clean package`
* `docker compose build`
* `docker compose up`
* Open `http://localhost:8080` in your browser
* Type `docker compose down` to shut down the app


To launch the app (Method B):
* Execute command string:
`docker run --rm -d -e POSTGRES_USER=user -e POSTGRES_PASSWORD=pass -e POSTGRES_DB=db -p 5432:5432 postgres`
* Run "MainWeb"
* Open `http://localhost:8080` in your browser
* Stop the container & MainWeb to terminate

Change history:

20.10.2022 First public version

Author: Pavel Volegov

Looking for a new job as a Java developer! 
Please contact me via Telegram 
`https://t.me/aaaooo888999`


