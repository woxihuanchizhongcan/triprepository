FROM openjdk:8u212-jre-alpine

COPY target/Trip-1.0-SNAPSHOT.jar /app.jar
COPY src/main/webapp /src/main/webapp

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "app.jar"]