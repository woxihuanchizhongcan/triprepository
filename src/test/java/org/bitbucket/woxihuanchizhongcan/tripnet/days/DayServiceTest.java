package org.bitbucket.woxihuanchizhongcan.tripnet.days;

import org.bitbucket.woxihuanchizhongcan.tripnet.dto.DayDto;
import org.bitbucket.woxihuanchizhongcan.tripnet.mapper.DayMapper;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;
import org.mockito.Mockito;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class DayServiceTest {
    DayMapper mapper = Mappers.getMapper(DayMapper.class);

    DayRepository repository = Mockito.mock(DayRepository.class);

    DayService service = new DayService(repository, mapper);

    DayInfo dayInfo = DayInfo.builder()
            .dayID(1)
            .date(LocalDate.of(2022, 1, 1))
            .tripID(1)
            .userID(1)
            .tripObjList("[{\"country\":\"China\",\"city1\":\"Bengbu\",\"name\":\"Magic Plane to Yekaterinburg\",\"address1\":\"Magic airport\",\"dayNum\":2,\"date\":[2022,11,16],\"type\":\"TRANSPORT\",\"city2\":\"Yekaterinburg\",\"address2\":\"Koltsovo airport\"}]")
            .build();
    DayDto dayDto = DayDto.builder()
            .dayID(1)
            .date(LocalDate.of(2022, 1, 1))
            .tripID(1)
            .userID(1)
            .tripObjList("[{\"country\":\"China\",\"city1\":\"Bengbu\",\"name\":\"Magic Plane to Yekaterinburg\",\"address1\":\"Magic airport\",\"dayNum\":2,\"date\":[2022,11,16],\"type\":\"TRANSPORT\",\"city2\":\"Yekaterinburg\",\"address2\":\"Koltsovo airport\"}]")
            .build();
    @Test
    public void addDay() {
        Mockito.when(repository.save(dayInfo)).thenReturn(dayInfo);
        Assert.assertEquals(dayDto, service.addDay(dayDto));

    }

    @Test
    public void getDaysWithTripID() {
        List<DayDto> dayDtos = new ArrayList<>();
        List<DayInfo> dayInfos = new ArrayList<>();
        List<DayDto> dayDtosTest = new ArrayList<>();
        for (int i = 1; i < 11; i++){
            DayDto dayDto = DayDto.builder()
                    .dayID(i)
                    .date(LocalDate.of(2022, 1, 1))
                    .tripID(i % 2)
                    .userID(1)
                    .build();
            dayDtos.add(dayDto);
            dayInfos.add(mapper.dtoToDayInfo(dayDto));
            if (dayDto.getTripID() == 0){
                dayDtosTest.add(dayDto);
            }
        }
        Mockito.when(repository.findAll()).thenReturn(dayInfos);
        Assert.assertEquals(dayDtosTest, service.getDaysWithTripID(0));
    }

    @Test
    public void getAll() {
        List<DayDto> dayDtos = new ArrayList<>();
        List<DayInfo> dayInfos = new ArrayList<>();

        for (int i = 1; i < 11; i++){
            DayDto dayDto = DayDto.builder()
                    .dayID(i)
                    .date(LocalDate.of(2022, 1, 1))
                    .tripID(i % 2)
                    .userID(1)
                    .build();
            dayDtos.add(dayDto);
            dayInfos.add(mapper.dtoToDayInfo(dayDto));
        }
        Mockito.when(repository.findAll()).thenReturn(dayInfos);
        Assert.assertEquals(dayDtos, service.getAll());
    }

    @Test
    public void delete() {
        Assert.assertEquals(dayDto, service.delete(dayDto));
    }
}