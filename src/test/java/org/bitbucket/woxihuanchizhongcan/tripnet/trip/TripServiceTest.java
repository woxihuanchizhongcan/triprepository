package org.bitbucket.woxihuanchizhongcan.tripnet.trip;

import org.bitbucket.woxihuanchizhongcan.tripnet.days.DayService;
import org.bitbucket.woxihuanchizhongcan.tripnet.dto.TripDto;
import org.bitbucket.woxihuanchizhongcan.tripnet.mapper.TripMapper;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;
import org.mockito.Mockito;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class TripServiceTest {
    TripMapper mapper = Mappers.getMapper(TripMapper.class);

    TripRepository repository = Mockito.mock(TripRepository.class);

    DayService dayService = Mockito.mock(DayService.class);
    TripService service = new TripService(repository, dayService, mapper);
    TripInfo tripInfo = TripInfo.builder()
            .tripID(1L)
            .userID(2L)
            .tripName("A")
            .fromCountry("Russia")
            .fromCity("Yekaterinburg")
            .toCountry("China")
            .toCity("Bengbu")
            .tripStartDate(LocalDate.of(2022, 1, 1))
            .tripEndDate(LocalDate.of(2022, 12, 31))

            .summary("BB")
            .build();
    TripInfo tripInfo2 = TripInfo.builder()
            .tripID(3L)
            .userID(4L)
            .tripName("A")
            .fromCountry("Russia")
            .fromCity("Yekaterinburg")
            .toCountry("China")
            .toCity("Bengbu")
            .tripStartDate(LocalDate.of(2023, 1, 1))
            .tripEndDate(LocalDate.of(2024, 12, 31))

            .summary("BB")
            .build();
    TripDto tripDto = TripDto.builder()
            .tripID(1L)
            .userID(2L)
            .tripName("A")
            .fromCountry("Russia")
            .fromCity("Yekaterinburg")
            .toCountry("China")
            .toCity("Bengbu")
            .tripStartDate(LocalDate.of(2022, 1, 1))
            .tripEndDate(LocalDate.of(2022, 12, 31))

            .summary("BB")
            .build();
    TripDto tripDto2 = TripDto.builder()
            .tripID(3L)
            .userID(4L)
            .tripName("A")
            .fromCountry("Russia")
            .fromCity("Yekaterinburg")
            .toCountry("China")
            .toCity("Bengbu")
            .tripStartDate(LocalDate.of(2023, 1, 1))
            .tripEndDate(LocalDate.of(2024, 12, 31))

            .summary("BB")
            .build();



    @Test
    public void addTrip() {
        Mockito.when(repository.save(tripInfo)).thenReturn(tripInfo);
        Assert.assertEquals(tripDto, service.addTrip(tripDto));
    }

    @Test
    public void getTripsWithUserID() {
        List<TripInfo> list = new ArrayList<>();
        list.add(tripInfo2);
        list.add(tripInfo);
        List<TripDto> list2 = new ArrayList<>();
        list2.add(tripDto);
        Mockito.when(repository.findAll()).thenReturn(list);
        Assert.assertEquals(list2, service.getTripsWithUserID(2L));
    }

    @Test
    public void getAll() {
        List<TripInfo> list = new ArrayList<>();
        list.add(tripInfo2);
        list.add(tripInfo);
        List<TripDto> list2 = new ArrayList<>();
        list2.add(tripDto2);
        list2.add(tripDto);
        Mockito.when(repository.findAll()).thenReturn(list);
        Assert.assertEquals(list2, service.getAll());

    }

    @Test
    public void getTrip() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.ofNullable(tripInfo));
        Assert.assertEquals(tripDto, service.getTrip(1L));
    }

    @Test
    public void delete() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.ofNullable(tripInfo));
        Assert.assertEquals(tripDto, service.delete(1L));

    }
}