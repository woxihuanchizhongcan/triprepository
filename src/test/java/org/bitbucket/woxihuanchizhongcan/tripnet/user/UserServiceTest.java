package org.bitbucket.woxihuanchizhongcan.tripnet.user;


import org.bitbucket.woxihuanchizhongcan.tripnet.dto.UserDto;
import org.bitbucket.woxihuanchizhongcan.tripnet.mapper.UserMapper;
import org.junit.Assert;
import org.junit.Test;
import org.mapstruct.factory.Mappers;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class UserServiceTest {
    UserMapper mapper = Mappers.getMapper(UserMapper.class);
    UserRepository userRepository = Mockito.mock(UserRepository.class);
    UserService userService = new UserService(userRepository, mapper);
    UserInfo userInfo = new UserInfo(
            1L,
            "A",
            "B",
            "A",
            "A",
            "A",
            "A"
    );
    UserDto userDto = new UserDto(
            1L,
            "A",
            "B",
            "A",
            "A",
            "A",
            "A"
    );

    @Test
    public void containsUser() {
        Mockito.when(userRepository.existsById(1L)).thenReturn(true);
        Assert.assertEquals(true, userService.containsUser(userDto));
    }

    @Test
    public void addUser() {
        Mockito.when(userRepository.save(userInfo)).thenReturn(userInfo);
        Assert.assertEquals(userDto, userService.addUser(userDto));

    }

    @Test
    public void getUserByID() {
        Mockito.when(userRepository.findById(1L)).thenReturn(Optional.ofNullable(userInfo));
        Assert.assertEquals(userDto, userService.getUserByID(1L));
    }

    @Test
    public void getAll() {
        List<UserInfo> list = new ArrayList<>();
        list.add(userInfo);
        List<UserDto> list2 = new ArrayList<>();
        list2.add(userDto);
        Mockito.when(userRepository.findAll()).thenReturn(list);
        Assert.assertEquals(list2, userService.getAll());
    }

    @Test
    public void getUserLoginPass() {
        List<UserInfo> list = new ArrayList<>();
        list.add(userInfo);
        Mockito.when(userRepository.findAll()).thenReturn(list);
        Assert.assertEquals(userDto, userService.getUser("A", "B"));
    }
    @Test
    public void getUserLogin() {
        List<UserInfo> list = new ArrayList<>();
        list.add(userInfo);
        Mockito.when(userRepository.findAll()).thenReturn(list);
        Assert.assertEquals(userDto, userService.getUser("A"));
    }
}