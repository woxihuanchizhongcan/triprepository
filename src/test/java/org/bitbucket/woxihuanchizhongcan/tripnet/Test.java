//package org.bitbucket.woxihuanchizhongcan.tripnet;
//
//import com.fasterxml.jackson.core.JsonProcessingException;
//import com.fasterxml.jackson.core.type.TypeReference;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.fasterxml.jackson.databind.jsontype.BasicPolymorphicTypeValidator;
//import com.fasterxml.jackson.databind.jsontype.PolymorphicTypeValidator;
//import org.bitbucket.woxihuanchizhongcan.tripnet.tripobj.TripObj;
//
//import java.util.ArrayList;
//
//public class Test {
//
//    @org.junit.jupiter.api.Test
//    public void bigTest() throws JsonProcessingException {
//        String jsonDataString = "{\n" +
//                "    \"vehicles\": [ {\n" +
//                "            \"country\" : \"s\",\n" +
//                "                    \"city1\" : \"s\",\n" +
//                "                    \"type\" : \"TRANSPORT\",\n" +
//                "                    \"name\" : \"s\",\n" +
//                "                    \"address1\" : \"s\",\n" +
//                "                    \"dayNum\" : 0,\n" +
//                "                    \"date\" : [ 2022, 10, 23 ],\n" +
//                "            \"city2\" : \"s\",\n" +
//                "                    \"address2\" : \"s\"\n" +
//                "        }, {\n" +
//                "            \"country\" : \"v\",\n" +
//                "                    \"city1\" : \"v\",\n" +
//                "                    \"type\" : \"FOOD\",\n" +
//                "                    \"name\" : \"v\",\n" +
//                "                    \"address1\" : \"v\",\n" +
//                "                    \"dayNum\" : 0,\n" +
//                "                    \"date\" : [ 2022, 10, 23 ]\n" +
//                "        }, {\n" +
//                "            \"country\" : \"34\",\n" +
//                "                    \"city1\" : \"3\",\n" +
//                "                    \"type\" : \"SIGHT\",\n" +
//                "                    \"name\" : \"3\",\n" +
//                "                    \"address1\" : \"3\",\n" +
//                "                    \"dayNum\" : 0,\n" +
//                "                    \"date\" : [ 2022, 10, 23 ]\n" +
//                "        } ]}";
//        PolymorphicTypeValidator ptv = BasicPolymorphicTypeValidator.builder()
//                .allowIfSubType("org.bitbucket.woxihuanchizhongcan.tripnet.tripobj.TripObj")
//                .allowIfSubType("java.util.ArrayList")
//                .build();
//        ObjectMapper mapper = new ObjectMapper();
//        mapper.activateDefaultTyping(ptv, ObjectMapper.DefaultTyping.NON_FINAL);
////        String jsonDataString = mapper.writeValueAsString(serializedFleet);
//        ArrayList<TripObj> tripObjs = mapper.readValue(jsonDataString, new TypeReference<ArrayList<TripObj>>(){});
//        System.out.println(jsonDataString);
//    }
//}
