package org.bitbucket.woxihuanchizhongcan.tripnet;

import org.bitbucket.woxihuanchizhongcan.tripnet.days.DayController;
import org.bitbucket.woxihuanchizhongcan.tripnet.days.DayService;
import org.bitbucket.woxihuanchizhongcan.tripnet.dto.TripDto;
import org.bitbucket.woxihuanchizhongcan.tripnet.dto.UserDto;
import org.bitbucket.woxihuanchizhongcan.tripnet.trip.TripService;
import org.bitbucket.woxihuanchizhongcan.tripnet.user.UserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * A controlled used for methods which logically don't fall into the other 3 controllers
 */
@RestController
@RequestMapping(value="/")
public class IndexController {
    UserService userService;
    TripService tripService;
    DayService dayService;
    DayController dayController;

    public IndexController(UserService userService, TripService tripService, DayService dayService, DayController dayController) {
        this.userService = userService;
        this.tripService = tripService;
        this.dayService = dayService;
        this.dayController = dayController;
    }

    /**
     * Show the main not-logged-in page
     * @return
     */
    @GetMapping
    public ModelAndView index() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/index1.jsp");
        return modelAndView;
    }

    /**
     * show the main logged-in page
     * @return
     */
    @GetMapping("/logged-in")
    public ModelAndView indexLoggedIn() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/index.jsp");
        return modelAndView;
    }

    /**
     * show the page with all trips by all users
     * @return
     */
    @GetMapping("/alltrips")
    public ModelAndView allTrips() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/alltrips.jsp");
        List<TripDto> all = tripService.getAll();
        Collections.sort(all);
        Collections.reverse(all);
        modelAndView.addObject("trips", all);
        modelAndView.addObject("userId", "0");
        return modelAndView;
    }

    /**
     * Sort trips on the alltrips page according to user parameters
     * @param userId
     * @param toCountry
     * @param toCity
     * @return
     */
    @GetMapping("/alltrips/sort")
    public ModelAndView allTripsSort(@RequestParam("userId") long userId,
                                    @RequestParam("toCountry") String toCountry,
                                     @RequestParam("toCity") String toCity) {
        String toCountryTrimmed = toCountry.trim();
        String toCityTrimmed = toCity.trim();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/alltrips.jsp");
        List<TripDto> tripsSorted;

        if (userId == 0) {
            tripsSorted = tripService.getAll();
        } else {

            String fullUserName = userService.getFullNameById(userId);
            modelAndView.addObject("fullUserName", fullUserName);

            tripsSorted = tripService.getTripsWithUserID(userId);
        }

        Collections.sort(tripsSorted);
        Collections.reverse(tripsSorted);

        if (!toCountryTrimmed.isEmpty()) {
            tripsSorted = tripsSorted.stream()
                    .filter(t -> t.getToCountry().equalsIgnoreCase(toCountryTrimmed))
                    .collect(Collectors.toList());
        }
        if (!toCityTrimmed.isEmpty()) {
            tripsSorted = tripsSorted.stream()
                    .filter(t -> t.getToCity().equalsIgnoreCase(toCityTrimmed))
                    .collect(Collectors.toList());
        }
        modelAndView.addObject("userId", userId);
        modelAndView.addObject("trips", tripsSorted);
        return modelAndView;
    }

    /**
     * See info of a trip selected in the alltrips menu - a method from DayController is used
     * @param tripId
     * @return
     */
    @GetMapping("/alltrips/onetrip")
    public ModelAndView ready(@RequestParam("chooseTripId") long tripId) {

        ModelAndView modelAndView = dayController.readyNew(tripId);

        TripDto tripDto = tripService.getTrip(tripId);
        String fullUserName = userService.getFullNameById(tripDto.getUserID());
        modelAndView.addObject("userId", tripDto.getUserID());
        modelAndView.addObject("fullUserName", fullUserName);
        return modelAndView;
    }

    /**
     * See user profile clicked from the trip description page (onetrip)
     * @param userId of a user whose trip was being inspected
     * @return
     */
    @GetMapping("/profile")
    public ModelAndView userProfile(@RequestParam("userId") long userId) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/userProfile.jsp");
        UserDto userDto = userService.getUserByID(userId);
        String fullName = userService.getFullNameById(userId);
        modelAndView.addObject("userId", userId);
        modelAndView.addObject("fullName", fullName);
        modelAndView.addObject("aboutMe", userDto.getAboutMe());
        return modelAndView;
    }

    /**
     * show menu for user to choose whether they want to edit their trips or see other people's trips
     * @return
     */
    @GetMapping("/what_trips")
    public ModelAndView whatTrips() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/what_trips.jsp");
        return modelAndView;
    }

    /**
     * show feedback form page
     * @return
     */
    @GetMapping("/feedback")
    public ModelAndView feedback() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/os_form.jsp");
        return modelAndView;
    }

    /**
     * show feedback response page (user input unprocessed for now)
     * @return
     */
    @GetMapping("/feedback/feedback_response*")
    public ModelAndView feedbackResponse() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/feedback_response.jsp");
        return modelAndView;
    }

    /**
     * show tech support page
     * @return
     */
    @GetMapping("/tech_support")
    public ModelAndView techSupport() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/teh_form.jsp");
        return modelAndView;
    }

    /**
     * show tech support response page (user input not processed for now)
     * @return
     */
    @GetMapping("/tech_support/tech_response*")
    public ModelAndView techResponse() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/tech_response.jsp");
        modelAndView.addObject("ticketId", (int) (Math.random() * 100));
        return modelAndView;
    }

    /**
     * show "about us" page
     * @return
     */
    @GetMapping("/about_us")
    public ModelAndView aboutUs() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/about_us.jsp");
        return modelAndView;
    }

    /**
     * show login page
     * @return
     */
    @GetMapping("/login")
    public ModelAndView login() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/login.jsp");
        return modelAndView;
    }

}
