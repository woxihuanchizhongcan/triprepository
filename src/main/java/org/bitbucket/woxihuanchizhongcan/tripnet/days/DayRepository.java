package org.bitbucket.woxihuanchizhongcan.tripnet.days;

import org.springframework.data.repository.CrudRepository;

/**
 * Standard CrudRepository for DayInfo
 */
public interface DayRepository extends CrudRepository<DayInfo, Long> {
}
