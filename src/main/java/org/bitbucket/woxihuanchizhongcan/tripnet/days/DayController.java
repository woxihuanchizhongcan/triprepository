package org.bitbucket.woxihuanchizhongcan.tripnet.days;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.bitbucket.woxihuanchizhongcan.tripnet.dto.DayDto;
import org.bitbucket.woxihuanchizhongcan.tripnet.dto.TripDto;
import org.bitbucket.woxihuanchizhongcan.tripnet.trip.TripService;
import org.bitbucket.woxihuanchizhongcan.tripnet.tripobj.TripObj;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value="/user/trip/days", consumes = MediaType.ALL_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class DayController {
    org.bitbucket.woxihuanchizhongcan.tripnet.days.DayService dayService;
    TripService tripService;
    public DayController(DayService dayService, TripService tripService) {
        this.dayService = dayService;
        this.tripService = tripService;
    }

    /**
     * summarizes a trip created by a user
     * In previous versions DayInfo was useful because each day was defined separately and there were no TripObj
     * Now DayInfo concept and related methods are very inefficient!
     * This: Json (from daysWithTipId) -> ArrayList<TripObj> dayTripObjList -> ArrayList<ArrayList<TripObj>> tripObjs -> json -> webpage
     * More efficient solution would be to keep ArrayList<ArrayList<TripObj>> tripObjs in TripInfo
     * But MAYBE DayInfo would be useful in the future to store data common for one given day
     * (weather info, general description, photos, ???)
     * @param genTripId - comes from js from day_edit_new.jsp
     * @return trip representation for user
     */
    @GetMapping("/ready_new")
    public ModelAndView readyNew(@RequestParam("genTripId") long genTripId
                              ) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/all-days_new.jsp");
        TripDto tripDto = tripService.getTrip(genTripId);

        modelAndView.addObject("tripName", tripDto.getTripName());
        modelAndView.addObject("tripSummary", tripDto.getSummary());

        List<DayDto> daysWithTripID = dayService.getDaysWithTripID(genTripId);
        System.out.println("ready: Days with trip ID: \n" + daysWithTripID);
        //tripObjs - list of "days" - "day" cut down to contain only a list of its tripObjs
        ObjectMapper objectMapper = JsonMapper.builder()
                .addModule(new JavaTimeModule())
                .build();
        ArrayList<ArrayList<TripObj>> tripObjs = tripObjsOfDays(daysWithTripID, objectMapper);
        System.out.println("objs :\n" + tripObjs);
        String jsonAllObjs = null;
        try {

            jsonAllObjs = objectMapper.writer().writeValueAsString(tripObjs);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        System.out.println("jsonAllObjs: " + jsonAllObjs);
        modelAndView.addObject("tripObjs", jsonAllObjs);
        return modelAndView;
    }



    /**
     *
     * processing input list formed by JS in the days' input page
     * @param tripObjs formed by JS at frontend
     * @param request HttpServletRequest
     */
    @PostMapping("/day_add")
    @ResponseBody
    public void dayAddNew(@RequestBody ArrayList<ArrayList<TripObj>> tripObjs,

                          HttpServletRequest request) {
        //request.setCharacterEncoding("utf8");
        System.out.println(tripObjs);
        HttpSession session = request.getSession();
        long tripId = (long) session.getAttribute("tripId");
        long userId = (long) session.getAttribute("userId");
        //removes days of a trip (tripDto data remains)
        clearTripDays(tripId);
        ObjectMapper objectMapper = JsonMapper.builder()
                .addModule(new JavaTimeModule())

                .build();
        LocalDate tripStartDate = tripService.getTrip(tripId).getTripStartDate();
        for (int i = 0; i < tripObjs.size(); i++) {
            ArrayList<TripObj> tripObjsOfThisDay = tripObjs.get(i);

            //each date computed separately in case one of tripObjsOfThisDay is null

            LocalDate thisDayDate = tripStartDate.plusDays(i);

            System.out.println("tripObjsOfThisDay: \n" + tripObjsOfThisDay);

            String json = null;
            try {
                //writerWithDefaultPrettyPrinter()
                json = objectMapper.writer().writeValueAsString(tripObjsOfThisDay);
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
           //future - js output: tripObjs[dayObj{date, tripObjs[]}]
                DayDto dayDto = DayDto.builder()
                        .tripID(tripId)
                        .userID(userId)
                        .date(thisDayDate)
                        .tripObjList(json)
                        .build();
                System.out.println("dayDto: \n" + dayDto);
                DayDto debug = addDay(dayDto);
                System.out.println("Debug: \n" + debug);
        }
//        LocalDate tripEndDate = tripObjs.get(tripObjs.size() - 1).get(0).getDate();
        LocalDate tripEndDate = tripStartDate.plusDays(tripObjs.size() - 1);
        //refreshing trip end date in case it is before or after the date user specified in the beginning
        TripDto trip = tripService.getTrip(tripId);
        trip.setTripEndDate(tripEndDate);
        tripService.addTrip(trip);
        }

    /**
     *
     * Displaying the main page for adding days
     * @param userId received from TripController, saved inside the session
     * @param tripId received from TripController, saved inside the session
     * @param dayN received from TripController - unused - legacy parameter - not removed for compatibility only
     * @param request HttpServletRequest
     * @return jsp page
     */
    @GetMapping
    public ModelAndView daysEditPage(@RequestParam("userId") long userId,
                             @RequestParam("tripId") long tripId,
                             @RequestParam("dayN") long dayN,
                             HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView();

        TripDto trip = tripService.getTrip(tripId);
        LocalDate tripStartDate = trip.getTripStartDate();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        //String dateStr = tripStartDate.format(formatter);
        //modelAndView.addObject("date", dateStr);
        modelAndView.addObject("date", tripStartDate);
        String toCountry = trip.getToCountry();
        modelAndView.addObject("country", toCountry);
        //if this trip already contains days, this means "edit" button is pressed in the trip menu
        Optional<List<DayDto>> daysOpt = Optional.of(getDaysWithTripID(tripId));
        daysOpt.ifPresent(days -> {
            ObjectMapper objectMapper = JsonMapper.builder()
                    .addModule(new JavaTimeModule())
                    .build();
            ArrayList<ArrayList<TripObj>> tripObjsOfDays = tripObjsOfDays(days, objectMapper);
            String json = null;
            try {

                json = objectMapper.writer().writeValueAsString(tripObjsOfDays);
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }

            modelAndView.addObject("daysForEdit", json);
        });

        HttpSession session = request.getSession();
        modelAndView.setViewName("/day_edit_new.jsp");
        session.setAttribute("tripId", tripId);
        session.setAttribute("userId", userId);
        return modelAndView;
    }

    /**
     * used for debug only - see all days in DB
     * @return add days in DB
     */
    @GetMapping("/all")
    public List<DayDto> getAll() {
        return dayService.getAll();
    }

    /**
     *
     * remove all days with given tripId
     * @param tripId - days of this trip are to be removed
     * @return true if any days were removed, false if not
     */
    public boolean clearTripDays(long tripId){
        return dayService.clearTripDays(tripId);
    }

    /**
     * Standard object add method
     * @param dto day-object to be added
     * @return added object with an id
     */
    public DayDto addDay(DayDto dto) {
        return dayService.addDay(dto);
    }

    /**
     * Find all days of a given trip
     * @param tripID trip to be examinded
     * @return all days (DayDto) of a trip
     */
    public List<DayDto> getDaysWithTripID(long tripID) {
        return dayService.getDaysWithTripID(tripID);
    }

    /**
     * Get all lists of TripObjs from Days and put them into a separate List of lists
     * @param daysWithTripID list of days to have lists of TripObjs extracted from
     * @param objectMapper mapper from the method which uses this method
     * @return List of Lists of TripObjs from Days
     */
    public ArrayList<ArrayList<TripObj>> tripObjsOfDays(List<DayDto> daysWithTripID, ObjectMapper objectMapper) {
        ArrayList<ArrayList<TripObj>> tripObjsOfDays = dayService.tripObjsOfDays(daysWithTripID, objectMapper);
        return tripObjsOfDays;
    }


}
