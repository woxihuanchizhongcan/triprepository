package org.bitbucket.woxihuanchizhongcan.tripnet.days;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.bitbucket.woxihuanchizhongcan.tripnet.dto.DayDto;
import org.bitbucket.woxihuanchizhongcan.tripnet.mapper.DayMapper;
import org.bitbucket.woxihuanchizhongcan.tripnet.tripobj.TripObj;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Service bean for DayInfo
 */
@Service
public class DayService {
    DayRepository dayRepository;
    DayMapper mapper;

    public DayService(DayRepository dayRepository, DayMapper mapper) {
        this.dayRepository = dayRepository;
        this.mapper = mapper;
    }

    /**
     * method for adding days
     * @param dto day to be added
     * @return added day with an id
     */
    public DayDto addDay(DayDto dto) {
        org.bitbucket.woxihuanchizhongcan.tripnet.days.DayInfo add = mapper.dtoToDayInfo(dto);
        org.bitbucket.woxihuanchizhongcan.tripnet.days.DayInfo saved = dayRepository.save(add);
        return mapper.dayInfoToDto(saved);
    }

    /**
     * see all days of a certain trip
     * @param tripID trip to be examined
     * @return lst of all days of a trip
     */
    public List<DayDto> getDaysWithTripID(long tripID) {
        ArrayList<DayDto> days = new ArrayList<>();
        for(org.bitbucket.woxihuanchizhongcan.tripnet.days.DayInfo dayInfo : dayRepository.findAll()){
            if (dayInfo.getTripID() == tripID) {
                days.add(mapper.dayInfoToDto(dayInfo));
            }
        }
        return days;
    }

    /**
     * used for debug only
     * @return all days in the DB
     */
    public List<DayDto> getAll() {
        ArrayList<DayDto> days = new ArrayList<>();
        for(org.bitbucket.woxihuanchizhongcan.tripnet.days.DayInfo dayInfo : dayRepository.findAll()){
            days.add(mapper.dayInfoToDto(dayInfo));
        }
        return days;
    }

    /**
     * day removal method
     * @param dto day to be removed
     * @return removed day
     */
    public DayDto delete(DayDto dto) {
        DayInfo dayInfo = mapper.dtoToDayInfo(dto);
        dayRepository.delete(dayInfo);
        return dto;
    }

    /**
     * Get all lists of TripObjs from Days and put them into a separate List of lists
     * @param daysWithTripID list of days to have lists of TripObjs extracted from
     * @param objectMapper mapper from the method which uses this method
     * @return List of Lists of TripObjs from Days
     */
    public ArrayList<ArrayList<TripObj>> tripObjsOfDays(List<DayDto> daysWithTripID, ObjectMapper objectMapper) {
        ArrayList<ArrayList<TripObj>> tripObjs = new ArrayList<>();
        daysWithTripID.forEach((n) -> {
            System.out.println("ready: n: \n" + n);
            String json = n.getTripObjList();
            //dayTripObjList - "day" - contains a list of tripObjs of 1 day
            ArrayList<TripObj> dayTripObjList = null;
            try {
                dayTripObjList = objectMapper.readValue(json, new TypeReference<ArrayList<TripObj>>(){});
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
            System.out.println("ready: dayTripObjList: \n" + n);
            tripObjs.add(dayTripObjList);

        });
        return tripObjs;
    }

    /**
     *
     * remove all days with given tripId
     * @param tripId - days of this trip are to be removed
     * @return true if any days were removed, false if not
     */
    public boolean clearTripDays(long tripId){
        List<DayDto> days = getDaysWithTripID(tripId);
        if (!days.isEmpty()) {
            days.forEach((n) -> {
                delete(n);
            });
            return true;
        }
        return false;
    }

    /**
     * dummy days
     */
    @PostConstruct
    public void init() {
        clearTripDays(1);
        addDay(DayDto.builder()
                .dayID(1)
                .tripID(1)
                .userID(1)
                .date(LocalDate.of(2022, 1 ,14))
                .tripObjList("[{\"country\":\"Russia\",\"city1\":\"Yekaterinburg\",\"name\":\"Koltsovo\",\"address1\":\"Koltsovo street 1\",\"dayNum\":0,\"date\":[2022,1,14],\"type\":\"STATION\"},{\"country\":\"Russia\",\"city1\":\"Yekaterinburg\",\"name\":\"Yekaterinburg-Beijing plane\",\"address1\":\"Koltsovo\",\"dayNum\":0,\"date\":[2022,1,14],\"type\":\"TRANSPORT\",\"city2\":\"Beijing\",\"address2\":\"Beijing International Airport\"},{\"country\":\"China\",\"city1\":\"Beijing\",\"name\":\"Bus\",\"address1\":\"Beijing International Airport\",\"dayNum\":0,\"date\":[2022,1,14],\"type\":\"TRANSPORT\",\"city2\":\"Beijing\",\"address2\":\"Beijing South Station Bus\"},{\"country\":\"China\",\"city1\":\"Beijing\",\"name\":\"Bullet train to Bengbu\",\"address1\":\"Beijing South Station\",\"dayNum\":0,\"date\":[2022,1,14],\"type\":\"TRANSPORT\",\"city2\":\"Bengbu\",\"address2\":\"Bengbu South Station\"},{\"country\":\"China\",\"city1\":\"Bengbu\",\"name\":\"Irbis Hotel\",\"address1\":\"Donghai Dadao 55\",\"dayNum\":0,\"date\":[2022,1,14],\"type\":\"HOTEL\"}]")
                .build());
        addDay(DayDto.builder()
                .dayID(2)
                .tripID(1)
                .userID(1)
                .date(LocalDate.of(2022, 1 ,15))
                .tripObjList("[{\"country\":\"China\",\"city1\":\"Bengbu\",\"name\":\"Mountain Temple\",\"address1\":\"Donghai Dadao 166\",\"dayNum\":1,\"date\":[2022,1,15],\"type\":\"SIGHT\"},{\"country\":\"China\",\"city1\":\"Bengbu\",\"name\":\"Lanzhou Lamian\",\"address1\":\"Xuehai Lu 45\",\"dayNum\":1,\"date\":[2022,1,15],\"type\":\"FOOD\"},{\"country\":\"China\",\"city1\":\"Benbu\",\"name\":\"Benbu Museum\",\"address1\":\"Museum Street 1\",\"dayNum\":1,\"date\":[2022,1,15],\"type\":\"SIGHT\"},{\"country\":\"China\",\"city1\":\"Bengbu\",\"name\":\"Night Food Street\",\"address1\":\"Street of Food 55\",\"dayNum\":1,\"date\":[2022,1,15],\"type\":\"FOOD\"},{\"country\":\"China\",\"city1\":\"\",\"name\":\"Irbis Hotel\",\"address1\":\"\",\"dayNum\":1,\"date\":[2022,1,15],\"type\":\"HOTEL\"}]")
                .build());
        addDay(DayDto.builder()
                .dayID(3)
                .tripID(1)
                .userID(1)
                .date(LocalDate.of(2022, 1 ,16))
                .tripObjList("[{\"country\":\"China\",\"city1\":\"Bengbu\",\"name\":\"Magic Plane to Yekaterinburg\",\"address1\":\"Magic airport\",\"dayNum\":2,\"date\":[2022,1,16],\"type\":\"TRANSPORT\",\"city2\":\"Yekaterinburg\",\"address2\":\"Koltsovo airport\"}]")
                .build());
    }
}
