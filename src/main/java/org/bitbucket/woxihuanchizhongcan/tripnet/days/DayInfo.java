package org.bitbucket.woxihuanchizhongcan.tripnet.days;

import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;

/**
 * Entity for saving information about each day of a trip
 * Annotation @Data not used because of a conflict
 */
@Data
@Builder
@Entity
@Table(name="days")
public class DayInfo {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial", name="day_id")
    private long dayID;
    @Column(name="trip_id")
    private long tripID;
    @Column(name="user_id")
    private long userID;
    @Column(name="date")
    private LocalDate date;

    @Column(name="trip_obj_list")
    private String tripObjList;

    public DayInfo() {
    }

    public DayInfo(long dayID, long tripID, long userID, LocalDate date, String tripObjList) {
        this.dayID = dayID;
        this.tripID = tripID;
        this.userID = userID;
        this.date = date;
        this.tripObjList = tripObjList;
    }

}
