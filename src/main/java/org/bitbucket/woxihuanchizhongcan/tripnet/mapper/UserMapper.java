package org.bitbucket.woxihuanchizhongcan.tripnet.mapper;


import org.bitbucket.woxihuanchizhongcan.tripnet.dto.UserDto;
import org.bitbucket.woxihuanchizhongcan.tripnet.user.UserInfo;
import org.mapstruct.Mapper;


/**
 * Auto-generated with MapStruct to map UserInfo to UserDto
 */
@Mapper(componentModel = "spring")
public interface UserMapper {
    public UserDto userInfoToDto(UserInfo userInfo);
    public UserInfo DtoToUserInfo(UserDto dto);

}
