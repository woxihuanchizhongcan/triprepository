package org.bitbucket.woxihuanchizhongcan.tripnet.mapper;

import org.bitbucket.woxihuanchizhongcan.tripnet.dto.TripDto;
import org.bitbucket.woxihuanchizhongcan.tripnet.trip.TripInfo;
import org.mapstruct.Mapper;


/**
 * Auto-generated with MapStruct to map TripInfo to TripDto
 */
@Mapper(componentModel = "spring")
public interface TripMapper {
    public TripDto tripInfoToDto(TripInfo tripInfo);
    public TripInfo DtoToTripInfo(TripDto dto);

}
