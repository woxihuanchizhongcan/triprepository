package org.bitbucket.woxihuanchizhongcan.tripnet.mapper;

import org.bitbucket.woxihuanchizhongcan.tripnet.days.DayInfo;
import org.bitbucket.woxihuanchizhongcan.tripnet.dto.DayDto;
import org.mapstruct.Mapper;

/**
 * Auto-generated with MapStruct to map DayInfo to DayDto
 */
@Mapper(componentModel = "spring")
public interface DayMapper {
    public DayDto dayInfoToDto(DayInfo dayInfo);
    public DayInfo dtoToDayInfo(DayDto dto);

}
