package org.bitbucket.woxihuanchizhongcan.tripnet.logger;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Used for logging app methods for each controller
 */
@Aspect
@Component
public class LoggerAspect {
    @Pointcut("execution(* org.bitbucket.woxihuanchizhongcan.tripnet.user.UserController.*(..))")
    public void userControllerPointCut() {
    }

    @Before("userControllerPointCut()")
    public void beforeMethod(JoinPoint joinPoint) {
        String methodName = joinPoint.getSignature().getName();
        List<String> args = getArgs(joinPoint);

        System.out.println("Method = " + methodName + " started; args = " + args);
    }

    @AfterReturning("userControllerPointCut()")
    public void afterMethod(JoinPoint joinPoint) {
        String methodName = joinPoint.getSignature().getName();
        List<String> args = getArgs(joinPoint);

        System.out.println("Method = " + methodName + " finished; args = " + args);
    }

    @AfterThrowing(value = "userControllerPointCut()", throwing = "ex")
    public void afterException(JoinPoint joinPoint, Throwable ex) {
        String methodName = joinPoint.getSignature().getName();
        List<String> args = getArgs(joinPoint);

        System.out.println("ERROR! Method = " + methodName + " failed; args = " + args + " error = " + ex);
    }

    @Pointcut("execution(* org.bitbucket.woxihuanchizhongcan.tripnet.trip.TripController.*(..))")
    public void tripControllerPointCut() {
    }

    @Before("tripControllerPointCut()")
    public void beforeMethodT(JoinPoint joinPoint) {
        String methodName = joinPoint.getSignature().getName();
        List<String> args = getArgs(joinPoint);

        System.out.println("Method = " + methodName + " started; args = " + args);
    }



    @AfterReturning("tripControllerPointCut()")
    public void afterMethodT(JoinPoint joinPoint) {
        String methodName = joinPoint.getSignature().getName();
        List<String> args = getArgs(joinPoint);

        System.out.println("Method = " + methodName + " finished; args = " + args);
    }

    @AfterThrowing(value = "tripControllerPointCut()", throwing = "ex")
    public void afterExceptionT(JoinPoint joinPoint, Throwable ex) {
        String methodName = joinPoint.getSignature().getName();
        List<String> args = getArgs(joinPoint);

        System.out.println("ERROR! Method = " + methodName + " failed; args = " + args + " error = " + ex);
    }

    @Pointcut("execution(* org.bitbucket.woxihuanchizhongcan.tripnet.days.DayController.*(..))")
    public void dayControllerPointCut() {
    }

    @Before("dayControllerPointCut()")
    public void beforeMethodD(JoinPoint joinPoint) {
        String methodName = joinPoint.getSignature().getName();
        List<String> args = getArgs(joinPoint);

        System.out.println("Method = " + methodName + " started; args = " + args);
    }



    @AfterReturning("dayControllerPointCut()")
    public void afterMethodD(JoinPoint joinPoint) {
        String methodName = joinPoint.getSignature().getName();
        List<String> args = getArgs(joinPoint);

        System.out.println("Method = " + methodName + " finished; args = " + args);
    }

    @AfterThrowing(value = "dayControllerPointCut()", throwing = "ex")
    public void afterExceptionD(JoinPoint joinPoint, Throwable ex) {
        String methodName = joinPoint.getSignature().getName();
        List<String> args = getArgs(joinPoint);

        System.out.println("ERROR! Method = " + methodName + " failed; args = " + args + " error = " + ex);
    }

    @Pointcut("execution(* org.bitbucket.woxihuanchizhongcan.tripnet.IndexController.*(..))")
    public void indexControllerPointCut() {
    }

    @Before("indexControllerPointCut()")
    public void beforeMethodI(JoinPoint joinPoint) {
        String methodName = joinPoint.getSignature().getName();
        List<String> args = getArgs(joinPoint);

        System.out.println("Method = " + methodName + " started; args = " + args);
    }



    @AfterReturning("indexControllerPointCut()")
    public void afterMethodI(JoinPoint joinPoint) {
        String methodName = joinPoint.getSignature().getName();
        List<String> args = getArgs(joinPoint);

        System.out.println("Method = " + methodName + " finished; args = " + args);
    }

    @AfterThrowing(value = "indexControllerPointCut()", throwing = "ex")
    public void afterExceptionI(JoinPoint joinPoint, Throwable ex) {
        String methodName = joinPoint.getSignature().getName();
        List<String> args = getArgs(joinPoint);

        System.out.println("ERROR! Method = " + methodName + " failed; args = " + args + " error = " + ex);
    }

    private List<String> getArgs(JoinPoint joinPoint) {
        List<String> args = new ArrayList<>();
        for (int i = 0; i < joinPoint.getArgs().length; i++) {
            Object argValue = joinPoint.getArgs()[i];
            args.add("arg." + i + "=" + argValue);
        }
        return args;
    }
}
