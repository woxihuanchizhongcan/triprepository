package org.bitbucket.woxihuanchizhongcan.tripnet.tripobj;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonTypeIdResolver;

import java.time.LocalDate;

//@Entity
//@Table(name = "trip_obj")
//@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@JsonTypeInfo(
        use = JsonTypeInfo.Id.CUSTOM,
        property = "type")
@JsonTypeIdResolver(FieldTypeResolver.class)
public abstract class TripObj {
//    @Id
//    long id;
    String country;
    String city1;
    String name;
    String address1;
//    String type;
    int dayNum;
    LocalDate date;

    public TripObj() {
    }

    public TripObj(String country, String city1, String name, String address1,
//                   String type,
                   int dayNum, LocalDate date) {
        this.country = country;
        this.city1 = city1;
        this.name = name;
        this.address1 = address1;
//        this.type = type;
        this.dayNum = dayNum;
        this.date = date;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity1() {
        return city1;
    }

    public void setCity1(String city1) {
        this.city1 = city1;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public int getDayNum() {
        return dayNum;
    }

    public void setDayNum(int dayNum) {
        this.dayNum = dayNum;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalDate getDate() {
        return date;
    }

}
