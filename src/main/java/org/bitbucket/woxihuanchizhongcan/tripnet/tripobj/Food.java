package org.bitbucket.woxihuanchizhongcan.tripnet.tripobj;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Food extends TripObj {
    final public String type = "FOOD";

    @Override
    public String toString() {
        return "Food (toString){" +
                "type='" + type + '\'' +
                ", country='" + country + '\'' +
                ", city1='" + city1 + '\'' +
                ", name='" + name + '\'' +
                ", address1='" + address1 + '\'' +
                ", dayNum=" + dayNum +
                ", date=" + date +
                '}';
    }
}