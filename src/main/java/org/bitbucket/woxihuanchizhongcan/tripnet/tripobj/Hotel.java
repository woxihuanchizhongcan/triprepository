package org.bitbucket.woxihuanchizhongcan.tripnet.tripobj;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Hotel extends TripObj {
    final public String type = "HOTEL";

    @Override
    public String toString() {
        return "Hotel (toString){" +
                "type='" + type + '\'' +
                ", country='" + country + '\'' +
                ", city1='" + city1 + '\'' +
                ", name='" + name + '\'' +
                ", address1='" + address1 + '\'' +
                ", dayNum=" + dayNum +
                ", date=" + date +
                '}';
    }
}
