package org.bitbucket.woxihuanchizhongcan.tripnet.tripobj;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Station extends TripObj {
    final public String type = "STATION";

    @Override
    public String toString() {
        return "Station (toString){" +
                "type='" + type + '\'' +
                ", country='" + country + '\'' +
                ", city1='" + city1 + '\'' +
                ", name='" + name + '\'' +
                ", address1='" + address1 + '\'' +
                ", dayNum=" + dayNum +
                ", date=" + date +
                '}';
    }
}
