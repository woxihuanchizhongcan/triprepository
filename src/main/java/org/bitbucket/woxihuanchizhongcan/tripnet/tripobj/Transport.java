package org.bitbucket.woxihuanchizhongcan.tripnet.tripobj;

import java.time.LocalDate;


public class Transport extends TripObj {

    final public String type = "TRANSPORT";
    String city2;
    String address2;

    @Override
    public String toString() {
        return "Transport (toString){" +
                "type='" + type + '\'' +
                ", city2='" + city2 + '\'' +
                ", address2='" + address2 + '\'' +
                ", country='" + country + '\'' +
                ", city1='" + city1 + '\'' +
                ", name='" + name + '\'' +
                ", address1='" + address1 + '\'' +
                ", dayNum=" + dayNum +
                ", date=" + date +
                '}';
    }

    public Transport() {
    }

    public Transport(String country, String city1, String name, String address1, int dayNum, LocalDate date, String city2, String address2) {
        super(country, city1, name, address1, dayNum, date);
        this.city2 = city2;
        this.address2 = address2;
    }

    public String getCity2() {
        return city2;
    }

    public void setCity2(String city2) {
        this.city2 = city2;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }
}
