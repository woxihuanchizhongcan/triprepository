package org.bitbucket.woxihuanchizhongcan.tripnet.tripobj;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DatabindContext;
import com.fasterxml.jackson.databind.JavaType;

import com.fasterxml.jackson.databind.jsontype.impl.TypeIdResolverBase;

/**
 * Used to de-serialize JSON into corresponding concrete TripObjs
 */
public class FieldTypeResolver extends TypeIdResolverBase {
    @Override
    public String idFromValue(Object o) {
        throw new RuntimeException("required for serialization only");
    }

    @Override
    public String idFromValueAndType(Object o, Class<?> aClass) {
        throw new RuntimeException("required for serialization only");
    }

    @Override
    public JavaType typeFromId(DatabindContext context, String id) {
        switch (id) {
            case "TRANSPORT": {
                return context.getTypeFactory().constructType(new TypeReference<Transport>() {
                });
            }
            case "FOOD":
            {
                return context.getTypeFactory().constructType(new TypeReference<Food>() {});
            }
            case "HOTEL":
            {
                return context.getTypeFactory().constructType(new TypeReference<Hotel>() {});
            }
            case "SIGHT":
            {
                return context.getTypeFactory().constructType(new TypeReference<Sightseeing>() {});
            }
            case "STATION":
            {
                return context.getTypeFactory().constructType(new TypeReference<Station>() {});
            }
            default: {
                throw new IllegalArgumentException(id + " not known");
            }
        }
    }

    @Override
    public JsonTypeInfo.Id getMechanism() {
        return JsonTypeInfo.Id.CUSTOM;
    }
}