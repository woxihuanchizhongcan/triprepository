package org.bitbucket.woxihuanchizhongcan.tripnet.trip;

import org.bitbucket.woxihuanchizhongcan.tripnet.days.DayService;
import org.bitbucket.woxihuanchizhongcan.tripnet.dto.DayDto;
import org.bitbucket.woxihuanchizhongcan.tripnet.dto.TripDto;
import org.bitbucket.woxihuanchizhongcan.tripnet.mapper.TripMapper;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
@Service
public class TripService {
    TripRepository tripRepository;
    DayService dayService;
    TripMapper mapper;

    public TripService(TripRepository tripRepository, DayService dayService, TripMapper mapper) {
        this.tripRepository = tripRepository;
        this.dayService = dayService;
        this.mapper = mapper;
    }

    /**
     * Add a trip to DB
     * @param tripDto trip to be added
     * @return added trip with ID
     */
    public TripDto addTrip(TripDto tripDto) {
        org.bitbucket.woxihuanchizhongcan.tripnet.trip.TripInfo add = mapper.DtoToTripInfo(tripDto);
        org.bitbucket.woxihuanchizhongcan.tripnet.trip.TripInfo saved = tripRepository.save(add);
        return mapper.tripInfoToDto(saved);
    }

    /**
     * get all trips of a user
     * @param userID user inder examination
     * @return all his trips
     */
    public List<TripDto> getTripsWithUserID(long userID) {
        ArrayList<TripDto> trips = new ArrayList<>();
        for(org.bitbucket.woxihuanchizhongcan.tripnet.trip.TripInfo tripInfo : tripRepository.findAll()){
            if (tripInfo.getUserID() == userID) {
                trips.add(mapper.tripInfoToDto(tripInfo));
            }
        }
        return trips;
    }

    /**
     * debug only
     * @return all trips in the DB
     */
    public List<TripDto> getAll() {
        ArrayList<TripDto> trips = new ArrayList<>();
        for(org.bitbucket.woxihuanchizhongcan.tripnet.trip.TripInfo tripInfo : tripRepository.findAll()){
            trips.add(mapper.tripInfoToDto(tripInfo));
        }
        return trips;
    }

    /**
     * Get trip DTO from a given ID
     * @param id
     * @return
     */
    public TripDto getTrip(long id) {
        org.bitbucket.woxihuanchizhongcan.tripnet.trip.TripInfo tripInfo = tripRepository.findById(id).get();
        return mapper.tripInfoToDto(tripInfo);
    }

    /**
     * remove a trip
     * @param deleteTripID trip to be removed
     * @return removed trip DTO
     */
    public TripDto delete(long deleteTripID) {
        List<DayDto> all = dayService.getAll();
        all.stream()
                .filter(t -> t.getTripID() == deleteTripID)
                .forEach(dayService::delete);
        TripInfo trip = tripRepository.findById(deleteTripID).get();
        tripRepository.deleteById(deleteTripID);
        return mapper.tripInfoToDto(trip);
    }

    /**
     * dummy trip
     */
    @PostConstruct
    public void init() {

        addTrip(TripDto.builder()
                .tripID(1)
                .userID(1)
                .tripName("Lovely Bengbu")
                .fromCountry("Russia")
                .fromCity("Yekaterinburg")
                .toCountry("China")
                .toCity("Bengbu")
                .tripStartDate(LocalDate.of(2022,1, 14))
                .tripEndDate(LocalDate.of(2022,1, 16))
                .summary("Epic trip")
                .build());
    }
}
