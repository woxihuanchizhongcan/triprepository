package org.bitbucket.woxihuanchizhongcan.tripnet.trip;

import org.bitbucket.woxihuanchizhongcan.tripnet.dto.TripDto;
import org.bitbucket.woxihuanchizhongcan.tripnet.dto.UserDto;
import org.bitbucket.woxihuanchizhongcan.tripnet.user.UserService;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
@RestController
@RequestMapping(value="/user/trip", consumes = MediaType.ALL_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class TripController {
    TripService tripService;
    UserService userService;

    public TripController(TripService tripService, UserService userService) {
        this.tripService = tripService;
        this.userService = userService;
    }

    /**
     * Display trip menu page
     * @param request
     * @param response
     * @return
     */
    @GetMapping
    public ModelAndView tripMenu (
                                  HttpServletRequest request,
                                  HttpServletResponse response) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/trip.jsp");
        return modelAndView;
    }

    /**
     * Receives and processes user information about a trip
     * params - from the corresponding HTML Form
     * @param tripName
     * @param fromCountry
     * @param fromCity
     * @param toCountry
     * @param toCity
     * @param tripStartDateString
     * @param tripEndDateString

     * @param summary
     * @param request
     * @param response
     */
    @PostMapping
    public void addTrip(
                        @RequestParam("tripName") String tripName,
                        @RequestParam("countryOut") String fromCountry,
                        @RequestParam("cityOut") String fromCity,
                        @RequestParam("countryIn") String toCountry,
                        @RequestParam("cityIn") String toCity,
                        @RequestParam("dateOut") String tripStartDateString,
                        @RequestParam("dateIn") String tripEndDateString,

                        @RequestParam("summary") String summary,
                         HttpServletRequest request,
                         HttpServletResponse response) {


        LocalDate tripStartDate = null;
        LocalDate tripEndDate = null;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        long userId = getUserIdFromSecurityContext();

        try {
            tripStartDate = LocalDate.parse(tripStartDateString, formatter);
        } catch (Exception ex){
            System.out.println("Incorrect date format! " + ex);
            System.out.println(ex);
        }
        try {
            tripEndDate = LocalDate.parse(tripEndDateString, formatter);
        } catch (Exception ex){
            System.out.println("Incorrect date format! " + ex);
            System.out.println(ex);
        }
        if (tripStartDate.isAfter(tripEndDate)){
            response.setStatus(response.SC_SEE_OTHER);
            response.setHeader("Location", "/user/trip/incorrect_date_order");
        } else {
            TripDto tripDto = TripDto.builder()
                    .userID(userId)
                    .tripName(tripName)
                    .fromCountry(fromCountry)
                    .fromCity(fromCity)
                    .toCountry(toCountry)
                    .toCity(toCity)
                    .tripStartDate(tripStartDate)
                    .tripEndDate(tripEndDate)

                    .summary(summary)
                    .build();

            tripDto = tripService.addTrip(tripDto);
            System.out.println("Trip " + tripDto.getTripName() + " added");
            response.setStatus(response.SC_SEE_OTHER);
            response.setHeader("Location", "/user/trip/days?userId=" +
                    userId + "&tripId=" + tripDto.getTripID() + "&dayN=" + 0);
        }

    }

    /**
     * Page shown if trip start date goes after the end date
     * @param request
     * @param response
     * @return
     */
    @GetMapping("/incorrect_date_order")
    public ModelAndView incorrectDateOrder (
                                  HttpServletRequest request,
                                  HttpServletResponse response) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/incorrect_date_order.jsp");
        return modelAndView;
    }

    /**
     * shows all trips of the current logged-in user for "edit trips" button in user menu
     * @param request
     * @param response
     * @return
     */
    @GetMapping("/trip-menu")
    public ModelAndView tripList(HttpServletRequest request,
                                 HttpServletResponse response) {
        long userId = getUserIdFromSecurityContext();
        List<TripDto> trips = tripService.getTripsWithUserID(userId);

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/trip-list.jsp");
        modelAndView.addObject("trips", trips);
        return modelAndView;
    }

    /**
     * on clicking the "edit" button in the menu with user's trips
     * redirects to corresponding trip page to edit the selected trip
     * @param request
     * @param response
     */
    @PostMapping("/trip-menu")
    public void chooseTrip (HttpServletRequest request,
                                    HttpServletResponse response) {
        String tripIDStr = request.getParameter("chooseTripId");
        long userId = getUserIdFromSecurityContext();
        response.setStatus(response.SC_SEE_OTHER);
        response.setHeader("Location", "/user/trip/days?userId=" +
                    userId + "&tripId=" + tripIDStr + "&dayN=0");
    }

    /**
     * on clicking the "delete" button in the menu with user's trips
     * @param tripID trip to be deleted
     * @param request
     * @param response
     */
    @GetMapping("/delete")
    public void deleteTrip (@RequestParam("deleteTripId") long tripID,
                            HttpServletRequest request,
                            HttpServletResponse response) {

        try {
            tripService.delete(tripID);

        } catch (EmptyResultDataAccessException exception) {
            System.out.println("No such trip!");
            exception.printStackTrace();
            response.setStatus(response.SC_SEE_OTHER);
            response.setHeader("Location", "/user/trip/trip-menu");
        }

        response.setStatus(response.SC_SEE_OTHER);
        response.setHeader("Location", "/user/trip/trip-menu");
    }

    /**
     * for debug purposes only - show all trips in DB
     * @return list of all trips
     */
    @GetMapping("/all")
    public List<TripDto> getAll() {
        return tripService.getAll();
    }

    /**
     * extracts the ID of the logged-in user
     * @return logged-in user's ID
     */
    private long getUserIdFromSecurityContext() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User credentials = (User) authentication.getPrincipal();
        String username = credentials.getUsername();
        UserDto user = userService.getUser(username);
        return user.getId();
    }

    /**
     * Method to add trip
     * @param userID
     * @param tripName
     * @param fromCountry
     * @param fromCity
     * @param toCountry
     * @param toCity
     * @param tripStartDate
     * @param tripEndDate

     * @param summary
     * @return
     */
    private TripDto addTrip(long userID,
                            String tripName,
                            String fromCountry,
                            String fromCity,
                            String toCountry,
                            String toCity,
                            LocalDate tripStartDate,
                            LocalDate tripEndDate,

                            String summary) {
        TripDto tripDto = TripDto.builder()
                .userID(userID)
                .tripName(tripName)
                .fromCountry(fromCountry)
                .fromCity(fromCity)
                .toCountry(toCountry)
                .toCity(toCity)
                .tripStartDate(tripStartDate)
                .tripEndDate(tripEndDate)

                .summary(summary)
                .build();

        return tripService.addTrip(tripDto);
    }

}
