package org.bitbucket.woxihuanchizhongcan.tripnet.trip;

import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;
@Data
@Entity
@Builder
@Table(name="trips")
public class TripInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial", name="trip_id")
    private long tripID;
    @Column(name="user_id")
    private long userID;
    @Column(name="trip_name")
    private String tripName;
    @Column(name="from_country")
    private String fromCountry;
    @Column(name="from_city")
    private String fromCity;
    @Column(name="to_country")
    private String toCountry;
    @Column(name="to_city")
    private String toCity;
    @Column(name="trip_start_date")
    private LocalDate tripStartDate;
    @Column(name="trip_end_date")
    private LocalDate tripEndDate;
    @Column(name="summary")
    private String summary;

    public TripInfo() {
    }

    public TripInfo(long tripID, long userID, String tripName, String fromCountry, String fromCity, String toCountry, String toCity, LocalDate tripStartDate, LocalDate tripEndDate, String summary) {
        this.tripID = tripID;
        this.userID = userID;
        this.tripName = tripName;
        this.fromCountry = fromCountry;
        this.fromCity = fromCity;
        this.toCountry = toCountry;
        this.toCity = toCity;
        this.tripStartDate = tripStartDate;
        this.tripEndDate = tripEndDate;
        this.summary = summary;
    }
}
