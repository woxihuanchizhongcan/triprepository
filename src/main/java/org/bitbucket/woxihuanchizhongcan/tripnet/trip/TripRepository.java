package org.bitbucket.woxihuanchizhongcan.tripnet.trip;

import org.springframework.data.repository.CrudRepository;
/**
 * Standard CrudRepository for TripInfo
 */
public interface TripRepository extends CrudRepository<TripInfo, Long> {
}
