package org.bitbucket.woxihuanchizhongcan.tripnet.dto;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;

/**
 * Dto for TripInfo
 */
@Data
@Builder
public class TripDto implements Comparable<TripDto>{
    private long tripID;
    private long userID;
    private String tripName;
    private String fromCountry;
    private String fromCity;
    private String toCountry;
    private String toCity;
    private LocalDate tripStartDate;
    private LocalDate tripEndDate;

    private String summary;

    public TripDto() {
    }

    public TripDto(long tripID, long userID, String tripName, String fromCountry, String fromCity, String toCountry, String toCity, LocalDate tripStartDate, LocalDate tripEndDate, String summary) {
        this.tripID = tripID;
        this.userID = userID;
        this.tripName = tripName;
        this.fromCountry = fromCountry;
        this.fromCity = fromCity;
        this.toCountry = toCountry;
        this.toCity = toCity;
        this.tripStartDate = tripStartDate;
        this.tripEndDate = tripEndDate;
        this.summary = summary;
    }

    @Override
    public int compareTo(TripDto o) {
        return tripStartDate.compareTo(o.getTripStartDate());
    }
}
