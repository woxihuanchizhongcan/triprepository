package org.bitbucket.woxihuanchizhongcan.tripnet.dto;

import lombok.Builder;
import lombok.Data;

/**
 * Dto for UserInfo
 */
@Data
@Builder
public class UserDto {

    private long id;
    private String login;
    private String password;
    private String firstName;
    private String lastName;
    private String secondName;
    private String aboutMe;

    public UserDto() {
    }

    public UserDto(long id, String login, String password, String firstName, String lastName, String secondName, String aboutMe) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.secondName = secondName;
        this.aboutMe = aboutMe;
    }


}
