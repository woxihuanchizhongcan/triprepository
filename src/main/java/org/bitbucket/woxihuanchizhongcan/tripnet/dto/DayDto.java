package org.bitbucket.woxihuanchizhongcan.tripnet.dto;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;

/**
 * Dto for DayInfo
 */
@Data
@Builder
public class DayDto {
    private long dayID;
    private long tripID;
    private long userID;
    private LocalDate date;
    private String tripObjList;
}
