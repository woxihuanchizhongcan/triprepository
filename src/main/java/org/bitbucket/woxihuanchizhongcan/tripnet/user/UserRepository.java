package org.bitbucket.woxihuanchizhongcan.tripnet.user;

import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<UserInfo, Long> {
}
