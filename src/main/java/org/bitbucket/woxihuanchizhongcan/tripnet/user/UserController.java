package org.bitbucket.woxihuanchizhongcan.tripnet.user;

import org.bitbucket.woxihuanchizhongcan.tripnet.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
@RequestMapping(value="/user", consumes = MediaType.ALL_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class UserController {

    private org.bitbucket.woxihuanchizhongcan.tripnet.user.UserService userService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/all")
    public List<UserDto> getAll() {
        return userService.getAll();
    }

    @GetMapping("/register*")
    public ModelAndView showRegister(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/register.jsp");
        return modelAndView;
    }

    @PostMapping("/register")
    public void register(@RequestParam("login") String login,
                         @RequestParam("password") String password,
                         @RequestParam("firstName") String firstName,
                         @RequestParam("lastName") String lastName,
                         @RequestParam("secondName") String secondName,
                         @RequestParam("aboutMe") String aboutMe,
                         HttpServletRequest request,
                         HttpServletResponse response) {
        if (userService.getUser(login) != null){
            response.setStatus(response.SC_SEE_OTHER);
            response.setHeader("Location", "/user/register?error=userExists");
        } else {
            UserDto user = UserDto.builder()
                    .login(login)
                    .password(password)
                    .firstName(firstName)
                    .lastName(lastName)
                    .secondName(secondName)
                    .aboutMe(aboutMe)
                    .build();
            user = userService.addUser(user);
            response.setStatus(response.SC_SEE_OTHER);
            response.setHeader("Location", "/logout");
        }
    }

    @GetMapping
    public ModelAndView showUserMenu(HttpServletRequest request,
                                     HttpServletResponse response){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User credentials = (User) authentication.getPrincipal();
        String username = credentials.getUsername();


        UserDto user = userService.getUser(username);
        ModelAndView modelAndView = new ModelAndView();

        modelAndView.setViewName("/user-menu.jsp");
        modelAndView.addObject("name", user.getFirstName());
        return modelAndView;
    }
    @GetMapping("/profile")
    public ModelAndView editProfileGet(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User credentials = (User) authentication.getPrincipal();
        String username = credentials.getUsername();
        UserDto user = userService.getUser(username);
        String myInfo = user.getAboutMe();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/profile.jsp");
        modelAndView.addObject("my_info", myInfo);
        return modelAndView;
    }

    @PostMapping("/profile")
    public void editProfilePost(@RequestParam("message") String aboutMe,
                                        HttpServletRequest request,
                                        HttpServletResponse response){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User credentials = (User) authentication.getPrincipal();
        String username = credentials.getUsername();
        UserDto user = userService.getUser(username);
        user.setAboutMe(aboutMe);
        userService.addUser(user);
        response.setStatus(response.SC_SEE_OTHER);
        response.setHeader("Location", "/user");

    }
}
