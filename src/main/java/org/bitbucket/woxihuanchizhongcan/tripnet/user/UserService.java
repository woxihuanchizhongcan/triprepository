package org.bitbucket.woxihuanchizhongcan.tripnet.user;

import org.bitbucket.woxihuanchizhongcan.tripnet.dto.UserDto;

import org.bitbucket.woxihuanchizhongcan.tripnet.mapper.UserMapper;


import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {
    private UserRepository userRepository;

    private UserMapper mapper;

    public UserService(UserRepository userRepository, UserMapper mapper) {
        this.userRepository = userRepository;
        this.mapper = mapper;

    }

    /**
     * checks whether a given user is in the DB
     * @param user to be checked
     * @return true for containing the user
     */
    public boolean containsUser(UserDto user){
        org.bitbucket.woxihuanchizhongcan.tripnet.user.UserInfo userInfo = mapper.DtoToUserInfo(user);
        boolean cont = userRepository.existsById(userInfo.getId());
        return cont;
    }

    /**
     * Adds a user into DB
     * @param user to be added
     * @return added user with assigned ID
     */
    public UserDto addUser(UserDto user) {
        org.bitbucket.woxihuanchizhongcan.tripnet.user.UserInfo add = mapper.DtoToUserInfo(user);
        org.bitbucket.woxihuanchizhongcan.tripnet.user.UserInfo saved = userRepository.save(add);
        return mapper.userInfoToDto(saved);
    }


    /**
     * retrieve a user from DB using a given ID
     * @param userID of the user to be retrieved
     * @return user with the ID
     */
    public UserDto getUserByID(Long userID) {
        return mapper.userInfoToDto(userRepository.findById(userID).get());
    }


    /**
     * Debug purposes only
     * @return all users from the DB
     */
    public List<UserDto> getAll() {
        ArrayList<UserDto> users = new ArrayList<>();
        for(org.bitbucket.woxihuanchizhongcan.tripnet.user.UserInfo userInfo : userRepository.findAll()){
            users.add(mapper.userInfoToDto(userInfo));
        }
        return users;

    }

    /**
     * return user with given login & password
     * @param login of the user to be retrieved
     * @param password of the user to be retrieved
     * @return user with given login & pass
     */
    public UserDto getUser(String login, String password){
        for(org.bitbucket.woxihuanchizhongcan.tripnet.user.UserInfo userInfo : userRepository.findAll()){
            if (userInfo.getLogin().equals(login) &&
            userInfo.getPassword().equals(password)){
                return mapper.userInfoToDto(userInfo);
            }
        }
        return null;
    }

    /**
     *
     * @param login of a user to be retrieved
     * @return user with the given login
     */
    public UserDto getUser(String login){
        for(UserInfo userInfo : userRepository.findAll()){
            if (userInfo.getLogin().equals(login)){
                return mapper.userInfoToDto(userInfo);
            }
        }
        return null;
    }

    /**
     *
     * @param userId ID of a user in question
     * @return that user's full name
     */
    public String getFullNameById (long userId) {
        UserDto userDto = getUserByID(userId);
        String fullName = userDto.getFirstName() + " " + userDto.getSecondName() + " " + userDto.getLastName();
        return fullName;
    }

    /**
     * Dummy users
     */
    @PostConstruct
    public void init() {
        addUser(UserDto.builder()
                .id(1)
                .login("test@test.test")
                .password("qqq")
                .firstName("TEST")
                .lastName("TESTOV")
                .secondName("TESTOVICZ")
                .aboutMe("I LOVE TESTING")
                .build());
        addUser(UserDto.builder()
                .id(2)
                .login("test2@test2.test2")
                .password("qqq")
                .firstName("Иван")
                .lastName("Петров")
                .secondName("Андреевич")
                .aboutMe("Просто Иван")
                .build());
        addUser(UserDto.builder()
                .id(3)
                .login("test3@test3.3")
                .password("qqq")
                .firstName("Ермолай")
                .lastName("Андреенко")
                .secondName("Альбертович")
                .aboutMe("Просто")
                .build());
    }

}
