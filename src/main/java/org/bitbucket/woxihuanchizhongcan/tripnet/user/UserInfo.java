package org.bitbucket.woxihuanchizhongcan.tripnet.user;

import lombok.Builder;
import lombok.Data;
import javax.persistence.*;

@Data
@Entity
@Builder
@Table(name="users")
public class UserInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private long id;
    @Column(name="login")
    private String login;
    @Column(name="password")
    private String password;
    @Column(name="first_name")
    private String firstName;
    @Column(name="last_name")
    private String lastName;
    @Column(name="second_name")
    private String secondName;
    @Column(name="about_me")
    private String aboutMe;


    public UserInfo() {
    }

    public UserInfo(long id, String login, String password, String firstName, String lastName, String secondName, String aboutMe) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.secondName = secondName;
        this.aboutMe = aboutMe;
    }

}
