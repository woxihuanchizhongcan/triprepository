package org.bitbucket.woxihuanchizhongcan.tripnet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * main method to launch the app
 */
@SpringBootApplication
public class MainWeb {
    public static void main( String[] args )
    {
        SpringApplication.run(MainWeb.class, args);
    }
}
