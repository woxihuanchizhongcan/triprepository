<%@ page contentType="text/html;charset=utf-8" %>
<html>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>User menu</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/user_menu_style.css">
</head>
<body>

<div class="large_frame">
    <div class="trip_caption"><P>User menu
        <hr><P></div>
    <form class="trip_form" name="feedback" method="POST" action="/login">

    <div style="margin-left:1%;transform: translate(0,3%)">
        <h2>Hello ${name}!</h2></div>
    <div class="trip_infofield">
        <div class="a2">
            <div class="a3">
                <p><div style="transform: translate(0,10%)">
                <p><a href="/user/profile">Edit profile</a><br></p>
                <p><a href="/user/trip">Add a trip</a><br></p>
                    <a href="/user/trip/trip-menu">Edit trips</a><br>
                </p>
                </div></div>
        </div>

    <p>

    <div class="a">
        <div class="a1">
            <div style="transform: translate(0,200%)">
                <p><a href="/logged-in">Back to the main page</a></p></div></div>
            </div>

        </div>
    </div>
</form>
</div>
</body>
</html>
