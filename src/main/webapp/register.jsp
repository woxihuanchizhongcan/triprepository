<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=utf-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Register</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/register.css">
</head>
<body>
<div class="large_frame">
<form class="trip_form" name="feedback" method="POST">
    <div class="trip_caption"><P>Register
        <P></div> <hr>
        <c:if test="${param.error != null}">
                    <div class="error" text-align="center">
                         This email is already registered
                    </div>
                </c:if>
    <div class="trip_infofield">Your email</div>
    <input type="email" name="login" required="required">
    <div class="trip_infofield">Password</div>
    <input type="text" name="password" required="required">
    <div class="trip_infofield">Name</div>
    <input type="firstName" name="firstName" required="required">
    <div class="trip_infofield">Surname</div>
    <input type="lastName" name="lastName" required="required">
    <div class="trip_infofield">Second Name</div>
    <input type="secondName" name="secondName" required="required">
    <div class="trip_infofield">About Me</div>
    <textarea name="aboutMe"></textarea>
    <p><input type="submit" name="submit_btn" value="Register"></p>
    <hr>
    <div class="a">
        <div class="a1">
            <div style="margin-left:22%;transform: translate(0,100%)">
                <p><a href="/logged-in">Back to the main page</a></p></div>
        </div>
    </div>
  </div>
</form>
</body>
</html>
