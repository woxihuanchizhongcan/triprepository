<%@ page contentType="text/html;charset=utf-8" %>
<html>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Error</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/teh_form_style.css">
</head>
<style class="text/css">
</style>
<body>
<div class="large_frame">
    <form class="guruweba_example_form" name="feedback" method="POST" action="/feedback.php">
        <div class="guruweba_example_caption">
            Trip start date can't be after the trip end date!
        </div>
        <div class="a">
            <div class="a1">
                <hr>
                <div style="margin-left:20px;transform: translate(0,-10%)">
                                                    <a href="/user"><h4>User menu</h4></a>
                                                </div>
                <div style="margin-left:20px;transform: translate(0,-10%)">
                    <a href="/logged-in"><h4>Back to the main page</h4></a>
                </div>

                </hr>
            </div>
        </div>

    </form>
</div>
</div>

</body>
</html>
</html>