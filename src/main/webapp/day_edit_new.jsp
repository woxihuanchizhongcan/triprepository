<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=utf-8" %>
<%@ page session="true" %>
<html>
<head>
    <meta charset="utf-8">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

    <script>

    $(document).ready(function(){

        var jspDate = "${date}";
        const tripStartDate = new Date (jspDate);

        $(document).find("h1").text("Day 1" + " (" + tripStartDate.toLocaleDateString() + ")");
        var wrapperBase = $("#container_0");
        var wrapCount = 0;
        var array = [];
        var allDays = [];
        allDays.push(array);
        //wrapper - product of clicking "new transport" etc
        $(wrapperBase).append('<div id="wrapper_' + wrapCount + '"><div>');
        var wrapper = $("#wrapper_" + wrapCount);
        //template1 - transport form template
        const template1 = document.getElementById("template1");
        const clone = template1.content.firstElementChild.cloneNode(true);
        //template0 - add TripObj table template
        const template0 = document.getElementById("table_0");//.lastChild;
        const clone0 = template0.cloneNode(true);
        var del_button = document.getElementById("delete_0");
        //hiding "delete last day entry" button
        del_button.style.display = "none";

        //is this request from the Edit menu?
        var daysForEdit = ${daysForEdit};
        if (Array.isArray(daysForEdit) && daysForEdit.length) {
            prepareForEdit(daysForEdit);
        }
        //if this trip already has days, we need to show them on this page
        function prepareForEdit(daysForEdit) {
            daysForEdit.forEach(drawTrip);
            function drawTrip(day, i) {
                if (Array.isArray(day) && day.length) {
                    //for day 0 we don't need to press the "add next day" button
                    if (i != 0) {
                        pressAddDayButton();
                    }
                    day.forEach(drawDay);
                } else {
                    pressAddDayButton();
                }
            }
            function drawDay(tripObj, j) {
                representTripObj(tripObj, null);
            }
        }
        //clears products of clicking "new transport" etc
        function clrWrap(wrap_n) {
            $("#wrapper_" + wrap_n).remove();
            $("#container_" + wrap_n).append('<div id="wrapper_' + wrap_n + '"><div>');
            //wrapper = $("#wrapper_" + wrapCount);
        }

        //remove TripObj parameters input form - for user convenience on clicking "cancel"
        $(document).on("click", ".cancel", function(event) {
              event.preventDefault();
              let position = this.id.search("_");
              var dayId = this.id.substr(position + 1);
              clrWrap(dayId);
              var newWrap = $("#wrapper_" + dayId);
              //$("wefwefwf").appendTo(newWrap);

        })

        //creating a form according to a given type of tripObj
        function makeForm(input, button) {
            let position = button.id.search("_");
            var dayId = button.id.substr(position + 1);
            clrWrap(dayId);
            var newWrap = $("#wrapper_" + dayId);

            var formClone = $(clone).clone();
            formClone.find(".tripObj_type").text("New entry - " + input);
            formClone.find(".dayNum").val(dayId);

            var tempDate = (addDays(tripStartDate, dayId)).toJSON() ;
            formClone.find(".date").val(tempDate);
            formClone.find(".type").val(input);
            //removing unused input areas
            if (input != "TRANSPORT") {
                formClone.find(".city2").remove();
                formClone.find(".address2").remove();
            }
            fixIds(formClone, dayId);
            formClone.appendTo(newWrap);

        }

        $(document).on("click", ".new_station", function(event) {
              event.preventDefault();
              button = this;
              makeForm("STATION", button);
        })

        $(document).on("click", ".new_hotel", function(event) {
          event.preventDefault();
          button = this;
          makeForm("HOTEL", button);
        })

        $(document).on("click", ".new_sight", function(event) {
          event.preventDefault();
          button = this;
          makeForm("SIGHT", button);
        })

        $(document).on("click", ".new_food", function(event) {
          event.preventDefault();
          button = this;
          makeForm("FOOD", button);
        })
        $(document).on("click", ".new_transport", function(event) {
           event.preventDefault();
           button = this;
           makeForm("TRANSPORT", button);
        })

    //add 1 tripObj into the array of 1 day on submitting the form
    $(document).on('submit', '.trip_form', function(event) {
        const data = new FormData(event.target);
        const formJSON = Object.fromEntries(data.entries());
        var form = this;
        representTripObj(formJSON, form);
    });

    //method which represents the tripObj on the web page,
    //be it a tripObj from the form or from a pre-given list
    function representTripObj(data, form) {
        //event.preventDefault();
        //is it a user clicking the form (form != null) or JSON data loading from the server (form = null)?
        if (form != null) {
            //find Id of this form
            //this Id is ID of this day
            let position = form.id.search("_");
            var dayId = form.id.substr(position + 1);
            var tripObjType = $(form).find(".type").val();
        } else {
            dayId = wrapCount;
            tripObjType = data.type;
        }

        var wrapperBaseNew = $("#container_" + dayId);
        var objTableNew = $("#table_" + dayId);

        const formJSON = data;
        //Adding an object to an array of objects (day n) of array of trip m
        allDays[dayId].push(formJSON);
        var objId = allDays[dayId].length - 1;
        //debug info
        const results = document.querySelector('.results pre');
        results.innerText = JSON.stringify(allDays[dayId], null, 2);
        //NOT ONLY TRANSPORT
        //a filled-in entry representation
        const template_tr = document.getElementById("template_transport_entry");
        const clone_tr = template_tr.content.firstElementChild.cloneNode(true);

        clone_tr.id = 'entry-' + dayId + '_' +  objId;
        clone_tr.querySelector(".details_button").id = 'details-' + dayId + '_' +  objId;
        var td_tr = clone_tr.querySelectorAll("td");
        td_tr[0].textContent = tripObjType + ": ";
        td_tr[1].textContent = formJSON.name;
        $(wrapperBaseNew).append(clone_tr);
        //clear old aux elements
        clrWrap(dayId);
        //show delete button
        var del_button = document.getElementById("delete_" + dayId);
        del_button.style.display = "block";
  }

  //show details button
  $(document).on('click', '.details_button', function(event) {
        event.preventDefault();
        let position0 = this.id.search("-");
        let position1 = this.id.search("_");
        var dayId = this.id.substring(position0 + 1, position1);
        var objId = this.id.substr(position1 + 1);
        var entry = $("#entry-" + dayId + "_" + objId);
        if (this.textContent == "Show Details") {
            $(entry).append('<pre id="details_info-' + dayId + '_' + objId + '">' + JSON.stringify(allDays[dayId][objId], null, 2) + '</pre>');
            this.textContent = "Hide Details";
        } else {
            this.textContent = "Show Details";
            $("#details_info-" + dayId + "_" + objId).remove();
        }

  });
    //remove a tripobj from a day on 'clicking delete last trip obj'
    $(document).on('click', '.delete_button', function(event) {
        event.preventDefault();
        let position = this.id.search("_");
        var dayId = this.id.substr(position + 1);
        var objId = allDays[dayId].length - 1;
        allDays[dayId].pop();
        const results = document.querySelector('.results pre');
        results.innerText = JSON.stringify(allDays[dayId], null, 2);
        $("#entry-" + dayId + "_" + objId).remove();
         if (objId == 0) {
            this.style.display = "none";
         }
    });

    //add_day_button - button for adding a new day
    $(document).on('click', '.add_day_button', function(event) {
        pressAddDayButton();
    });

    function pressAddDayButton() {
        //event.preventDefault();
        //wrapCount - latest DayId
        wrapCount++;
        $('#days-body_0').append('<div class="container" id="container_' + wrapCount + '"> </div>');
        const newContainer = $("#container_" + wrapCount);
        var frameClone = $("#large-frame_0").clone(); //.appendTo(newContainer);
        fixIds(frameClone, wrapCount);
        frameClone.find("h1").text("Day " + (wrapCount + 1) + " (" + (addDays(tripStartDate, wrapCount)).toLocaleDateString() + ")");
        frameClone.appendTo(newContainer);
        //wrapper for temporary interface elements
        $(newContainer).append('<div id="wrapper_' + wrapCount + '"><div>');
        var wrapper = $("#wrapper_" + wrapCount);
        allDays.push(new Array());
        $('#days-body_0').append('<button class="delete_button" id="delete_' + wrapCount + '">Delete last day entry for day ' + (wrapCount + 1) + '</button>');
        var new_del_button = document.getElementById("delete_" + wrapCount);
        //hiding "delete last day entry" button (it is visible by default)
        new_del_button.style.display = "none";
        //showing "delete last day" button (it is hidden by default)
        var delDayButton = document.getElementById("delDay_0");
        delDayButton.style.display = "block";
    }

    //del_day_button - button for deleting the last day
    $(document).on('click', '.del_day_button', function(event) {
        allDays.pop();
        $("#container_" + wrapCount).remove();
        $("#delete_" + wrapCount).remove();
        wrapCount--;
        if (wrapCount == 0) {
            this.style.display = "none";
        }
    });

    //add days to date - returns date
    function addDays(date, days) {
        var result = new Date(date);
        days = parseInt(days);
        result.setDate(result.getDate() + days);
        //return result.toLocaleDateString();
        return result;
    }

    //changes ids of an element and its children as per requirement
    function fixIds(elem, cntr) {
        $(elem).find("[id]").add(elem).each(function() {
            let position = this.id.search("_");
            this.id = this.id.substring(0, position + 1) + cntr;

        })
    }

    //final submission of all days!
    $(document).on('click', '#send_1', function(event) {
        event.preventDefault();
        $.post({
            url: "/user/trip/days/day_add",
            data: JSON.stringify(allDays),
            contentType: 'application/json; charset=utf-8'
        })
            .done(function (status) {
               $(location).attr('href','/user/trip/days/ready_new?genTripId=' + '${sessionScope.tripId}');
            });
    });

});

</script>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Trip creation</title>
<!--    <link rel="stylesheet" href="css/days_style.css">-->
</head>
<style class="text/css">
</style>
<body>
<div class="a2">
            <div class="a3">
                <table class="center">
                <col width="250px" />
                <col width="250px" />
                <col width="250px" />
                <tr>
                    <td><a href="/user">My trips</a></td>
                    <td><a href="/alltrips">Trips around the world</a></td>
                    <td><a href="/logged-in">Back to the main page</a></td>
                </tr>
                </table>

            </div>
</div>
<div class="days-body" id="days-body_0">
    <div class="large_frame" id="large-frame_0">
        <h1>День 1 - </h1>
        <div class="trip_caption"><h2>Add trip items</h2></div>
        <table class="obj_table" id="table_0">
            <tr>
                <td>
                    <button class="new_transport" id="nt_0" >New Transport</button>
                </td>
                <td>
                    <button class="new_station" id="nst_0" >New Station</button>
                </td>
                <td>
                    <button class="new_hotel" id="nh_0" >New Hotel</button>
                </td>
                <td>
                    <button class="new_sight" id="nsi_0" >New Sightseeing</button>
                </td>
                <td>
                    <button class="new_food" id="nf_0" >New Restaurant</button>
                </td>
                <td>
                    <button class="cancel" id="cancel_0" >Cancel</button>
                </td>
            </tr>
        </table>
    </div>
    <div class="container" id="container_0"> </div>
    <button class="delete_button" id="delete_0">Delete last day entry for day 1</button>
</div>
<button class="add_day_button" id="addDay_0">Add next day</button>
<button class="del_day_button" id="delDay_0" hidden>Remove last day</button>
<button class="send_button" id="send_1">Submit trip data</button>

<div class="results">
    <h2>Form Data</h2>
    <pre></pre>
</div>

<template id="template1">
    <form class="trip_form" id="trForm_0">
        <h3 class="tripObj_type"></h3>
        <table>
            <tr>
                    <td><h3>Country:</h3></td>

                <td><textarea name="country">${country}</textarea></td>
            </tr>
            <tr>

                    <td><h3>Name:</h3></td>

                <td><textarea name="name">${name}</textarea></td>
            </tr>
            <tr>

                    <td><h3>City:</h3></td>

                <td><textarea name="city1">${city1}</textarea></td>
            </tr>
            <tr>

                    <td><h3>Address:</h3></td>

                <td><textarea name="address1">${address1}</textarea></td>
            </tr>
            <tr class="city2">

                    <td><h3>Destination city: </h3></td>

                <td><textarea name="city2" >${city2}</textarea></td>
            </tr>
            <tr class="address2">

                    <td><h3>Destination address:</h3></td>

                <td><textarea name="address2">${address2}</textarea></td>
            </tr>

        </table>
        <input type="submit" value="Добавить">
        <input class="dayNum" name="dayNum" value="" hidden>
        <input class="date" name="date" value="" hidden>
        <input class="type" name="type" value="TRANSPORT" hidden>
    </form>
</template>

<template id="template_transport_entry">
    <table>
        <tr>
            <td class="record"></td>
            <td></td>
            <td><button class="details_button" id="details_new">Show Details</button></td>
        </tr>
    </table>

</template>
</body>
</html>
