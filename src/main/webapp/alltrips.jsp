<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=utf-8" %>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>View trips</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/alltrips.css">
</head>
<style class="text/css">
</style>
<body>
        <div class="a2">
            <div class="a3">
                <table class="center">
                <col width="250px" />
                <col width="250px" />
                <col width="250px" />
                <tr>
                    <td><a href="/user">My trips</a></td>
                    <td><a href="/alltrips">Trips around the world</a></td>
                    <td><a href="/logged-in">Back to the main page</a></td>
                </tr>
                </table>

            </div>
        </div>

        <div style="margin-top: 1%; text-align: center; transform: translate(0,120%)">
            <h1>View trips</h1>

        </div>
        <div style="margin-top: 1%; text-align: center; transform: translate(0,120%)">
            <h2>${fullUserName}</h2>
        </div>
        <div style="margin-top: 1%; text-align: center; transform: translate(0,120%)">
            <h2>Filter:</h2>
        </div>

   <div class="trip_form"><form class= method="GET" action="/alltrips/sort">
        <div class="trip_infofield">Destination country:</div>
        <input type="text" name="toCountry">
        <div class="trip_infofield">Destination city:</div>
        <input type="text" name="toCity"><br>
        <input type="submit" value="Apply filter">
        <input name="userId" value=${userId} hidden>
    </form></div>
	<c:forEach items="${trips}" var="trip">
	    <div class="trip_form">
	        <form method="GET" action="/alltrips/onetrip">
                Trip name: ${trip.tripName} <br>
                From: ${trip.fromCountry}, ${trip.fromCity}<br>
                To: ${trip.toCountry}, ${trip.toCity}<br>
                Start date: ${trip.tripStartDate}<br>End date: ${trip.tripEndDate}<br>
                <input type="submit" value="View trip">
                <input name="chooseTripId" value=${trip.tripID} hidden><br>
            </form>
        </div>
    </c:forEach>
</div>
</div>

</body>
