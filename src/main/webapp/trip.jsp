<%@ page contentType="text/html;charset=utf-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>The Trip</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/trip.css">
</head>
<style type="text/css"></style>
<body>
<div class="large_frame">
    <form class="trip_form" name="feedback" method="POST" action="">
        <div class="trip_caption"><P>The Trip
            <P></div>
        <div class="trip_infofield">Trip name</div>
        <input type="text" name="tripName" required="required">
        <div>Country of departure</div>
        <input type="text" name="countryOut" required="required">
        <div>City of departure</div>
        <input type="text" name="cityOut" required="required">
        <div>Country of arrival</div>
        <input type="text" name="countryIn" required="required">
        <div>City of arrival</div>
        <input type="text" name="cityIn" required="required">
        <div>Departure date</div>
        <input type="date" name="dateOut" placeholder="dd-mm-yyyy" value="" required="required">
        <div>Return date</div>
        <input type="date" name="dateIn" placeholder="dd-mm-yyyy" value="" required="required">
        <div class="trip_infofield">Trip short description</div>
        <textarea name="summary"></textarea>
        <input type="submit" name="submit_btn" value="Submit">
        <br>
        <br>
        <div class="a">
            <div class="a1">
                <div style="text-align: center; ">
                   <a href="/user" >My Trips</a>
                </div>
                <br>
                <div style="text-align: center; ">
                    <a href="/logged-in">Back to the main page</a>
                </div>

            </div>
        </div>
</div>


</form>
</body>
</html>