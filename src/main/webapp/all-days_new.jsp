<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=utf-8" %>
<html>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Trip Information</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/all_days_style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

    <script>
    $(document).ready(function(){
        var allDays = ${tripObjs};

        allDays.forEach(drawTrip);
    });

    function drawTrip(day, i) {
        out = $("#out0");
        if (Array.isArray(day) && day.length) {

            tripObj0 = day[0];

            date = new Date (tripObj0.date);
            $(out).append('<h1>Day ' + (i + 1) + ' (' + date.toLocaleDateString() + ')</h1>');
            day.forEach(drawDay);

            function drawDay(item, j) {
                //out = $("#out0");
                $(out).append('<table id="obj-' + i + '_' + j + '">');
                $(out).append('<tr><td><h3>' + item.type + '</h3></td></tr>');
                $(out).append('<tr><td>Name: </td><td>' + item.name + '</td></tr>');
                $(out).append('<tr><td>Country: </td><td>' + item.country + '</td></tr>');

                switch(item.type) {
                    case "TRANSPORT":
                        $(out).append('<tr><td>Departure City: </td><td>' + item.city1 + '</td></tr>');
                        $(out).append('<tr><td>Departure Address: </td><td>' + item.address1 + '</td></tr>');
                        $(out).append('<tr><td>Destination City: </td><td>' + item.city2 + '</td></tr>');
                        $(out).append('<tr><td>Destination Address: </td><td>' + item.address2 + '</td></tr>');
                    break;
                    default:
                        $(out).append('<tr><td>City: </td><td>' + item.city1 + '</td></tr>');
                        $(out).append('<tr><td>Address: </td><td>' + item.address1 + '</td></tr>');
                }
                $(out).append('</table><br>');
            }
        } else {
            $(out).append('<h1>Day ' + (i + 1) + '</h1>');
            $(out).append('<b>No info!</b>');
        }
    }
    </script>
</head>
<style class="text/css">
</style>
<body>
<div class="a">
        <div class="a1">
                <table class="center">
                <col width="250px" />
                <col width="250px" />
                <col width="250px" />
                <tr>
                    <td><a href="/user">My trips</a></td>
                    <td><a href="/alltrips">Trips around the world</a></td>
                    <td><a href="/logged-in">Back to the main page</a></td>
                </tr>
                </table>

            </div>
</div>
<div style="margin-left:300px;transform: translate(0,5%)">
    <h1>Trip Information</h1>
    <h2>Trip Name: ${tripName}</h2>
    <c:if test="${param.chooseTripId != null}">
        <form method="GET" action="/profile">
            <h3>Traveller name: ${fullUserName} </h3>
                    <input type="submit" value="See user profile">
                    <input name="userId" value=${userId} hidden>
        </form>
        <br>
   </c:if>
   <h3>Short description</h3>
       ${tripSummary}
       <hr>
    <div class="output" id="out0"></div>

</div>
<hr>



</body>
</html>