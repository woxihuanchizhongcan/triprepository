<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=utf-8" %>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Trip Menu</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/what-trips.css">
</head>
<style class="text/css">
</style>
<body>
<div class="large_frame">

        <div class="guruweba_example_caption">
            Trip Menu
            <br>
                <hr>
        </div>
        <div class="a">
                    <div class="a1">

                        <div style="text-align: center; transform: translate(0,-30%)">
                            <a href="/alltrips" style="text-decoration: none;">All trips around the world</a>
                        </div>
                        <br>
                        <div style="text-align: center; transform: translate(0,-20%)">
                            <a href="/user" style="text-decoration: none;">My trips</a>
                        </div>
                        <br>
                        <div style="text-align: center; transform: translate(0,-10%)">
                            <a href="/logged-in" style="text-decoration: none;">Back to the main page</a>
                        </div>

                    </div>
                </div>



</body>
</html>