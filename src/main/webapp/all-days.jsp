<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=utf-8" %>
<html>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Trip Information</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/all_days_style.css">
</head>
<style class="text/css">
</style>
<body>
<div style="margin-left:300px;transform: translate(0,5%)">
    <h1>Trip Information</h1>
    <h2>Trip Name: ${tripName}</h2>
    <c:if test="${param.chooseTripId != null}">
        <form method="GET" action="/profile">
            <h3>Traveller name: ${fullUserName} </h3>
                    <input type="submit" value="See profile">
                    <input name="userId" value=${userId} hidden>
        </form>
        <br>
   </c:if>
    <h3>Short summary</h3>
    ${tripSummary}
    <h3>Currency exchange situation</h3>
    ${currEx}</div>
    <hr>
    <c:forEach items="${days}" var="day">
    <div style="margin-left:300px;transform: translate(0,5%)">
    <table>

            <h2>${day.date} </h2>
        </tr>
        <tr>
           <td> <h4>Место ночлега вечером этого дня:</h4></td>
            <td>${day.sleep}</td>
        </tr>
        <tr>
            <td><h4>Где кушали:</h4></td>
            <td>${day.food}</td>
        </tr>
        <tr>
            <td><h4>Какие места посетили:</h4></td>
            <td>${day.visit}</td>
        </tr>
        <tr>
            <td><h4>На чем передвигались:</h4></td>
            <td>${day.transport}</td>
        </tr>
    </table>
</div>
</div>
<hr>
</c:forEach>
<div style="margin-left:300px; margin-bottom:3%; transform: translate(0,5%)">
    <div class="a">
        <div class="a1">
        <a href="/user">Мои поездки</a><br><br>
        <a href="/logged-in">На главную страницу</a>
        </div>
    </div>
</div>

</body>
</html>