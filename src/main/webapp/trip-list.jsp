<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=utf-8" %>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Trip Select</title>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/css/alltrips.css">
</head>
<style class="text/css">
</style>
<body>
        <div class="a2">
            <div class="a3">
                <table class="center">
                <col width="250px" />
                <col width="250px" />
                <col width="250px" />
                <tr>
                    <td><a href="/user">My trips</a></td>
                    <td><a href="/alltrips">Trips all around the world</a></td>
                    <td><a href="/logged-in">Back to the main page</a></td>
                </tr>
                </table>

            </div>
        </div>
 <div style=" text-align: center; transform: translate(0,120%)">
    <h1>Выбор поездки</h1>

 </div>
    <br>
	<c:forEach items="${trips}" var="trip">
	<div class="trip_form">
	        <form method="POST" action="">
                Trip name: ${trip.tripName} <br>
                From: ${trip.fromCountry}, ${trip.fromCity}<br>
                To: ${trip.toCountry}, ${trip.toCity}<br>
                Start date: ${trip.tripStartDate}<br>End date: ${trip.tripEndDate}<br>
                <input type="submit" value="Edit">
                <input name="chooseTripId" value=${trip.tripID} hidden><br>
            </form>
            <form method="GET" action="delete">
                <input type="submit" value="Delete">
                <input name="deleteTripId" value=${trip.tripID} hidden><br>
            </form>
            <hr>
            </div>
    </c:forEach>


</body>
</html>