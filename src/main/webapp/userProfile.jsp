<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=utf-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>${fullName}</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/see_user_profile.css">
</head>
<body>
<div class="large_frame">
<form class="trip_form" name="f" method="GET" action="/alltrips/sort">

    <h1>${fullName}</h1>

     <h2>About me</h2>
    <p>${aboutMe}</p>


    <input type="submit" value="See user trips">
    <input name="userId" value=${userId} hidden>
    <input name="toCountry" value="" hidden>
    <input name="toCity" value="" hidden><br>

    <div class="a" style="text-align: center;">
        <div class="a1">
            <hr>

                <p><a href="/">Back to the main page</a></p>

            </hr>
        </div>
    </div>
</div>
</form>
</body>
</html>