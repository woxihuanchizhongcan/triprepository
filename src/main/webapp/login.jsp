<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=utf-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Log in</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/login.css">
</head>
<body>
<div class="large_frame">
<form class="trip_form" name="f" method="POST" action="/perform_login">

    <div class="trip_caption"><P>Log in
        <P></div>
        <c:if test="${param.error != null}">
            <div class="error" text-align="center">
                 Incorrect login or password
            </div>
        </c:if>
    <div class="trip_infofield">Username</div>
    <input type="email" name="username" required="required">
    <div class="trip_infofield">Password</div>
    <input type="text" name="password" required="required">
    <div class="trip_infofield"></div>
    <p><input type="submit" name="submit_btn" font-family="Arial Black" value="Log in"></p>
    <div class="a" style="text-align: center;">
        <div class="a1">
            <hr>

                <a href="/">Back to the main page</a>

            </hr>
        </div>
    </div>
</div>
</form>
</body>
</html>