<%@ page contentType="text/html;charset=utf-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Feedback form</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/os _form_style.css">
</head>
<style class="text/css">
</style>
<body>
<div class="large_frame">
    <form class="guruweba_example_form" name="feedback" method="GET" action="/feedback/feedback_response">
        <div class="guruweba_example_caption">Feedback form</div>
        <div class="guruweba_example_infofield">Message topic</div>
        <select name="theme" required="required">
            <option value="">Please select</option>
            <option>A question regarding the operation of the service</option>
            <option>How to improve our service / Recommendations</option>
        </select>
        <div>Your Name</div>
        <input type="text" name="name" required="required">
        <div>Your email</div>
        <input type="email" name="email" required="required">
        <div>The Message</div>
        <textarea name="message"></textarea>
        <input type="submit" name="submit_btn" value="Send">
        <div class="a">
            <div class="a1">
                <hr>
                <div style="margin-left:20px;transform: translate(0,-10%)">
                    <a href="/"><h4>Back to the main page</h4></a>
                </div>
                </hr>
            </div>
        </div>

    </form>
</div>

</body>
</html>
