--liquibase formatted sql

--changeset pavel:1
CREATE TABLE IF NOT EXISTS public.users (
	id serial8 NOT NULL,
	login varchar NULL,
	"password" varchar NULL,
	first_name varchar NULL,
	last_name varchar NULL,
	second_name varchar NULL,
	about_me varchar NULL,
	CONSTRAINT users_pk PRIMARY KEY (id)
);

--changeset pavel:2
CREATE TABLE IF NOT EXISTS public.trips (
	trip_id serial8 NOT NULL,
	user_id bigint NULL,
	trip_name varchar NULL,
	from_country varchar NULL,
	from_city varchar NULL,
	to_country varchar NULL,
	to_city varchar NULL,
	trip_start_date DATE NULL,
	trip_end_date DATE NULL,
	curr_ex varchar NULL,
	summary varchar NULL,
	CONSTRAINT trips_pk PRIMARY KEY (trip_id)
);

--changeset pavel:3
CREATE TABLE IF NOT EXISTS public.days (
    day_id serial8 NOT NULL,
	trip_id bigint NULL,
	user_id bigint NULL,
	"date" DATE NULL,
	sleep varchar NULL,
	food varchar NULL,
	visit varchar NULL,
	transport varchar NULL,
	CONSTRAINT days_pk PRIMARY KEY (day_id)
);

--changeset pavel:4
ALTER TABLE public.days ADD tripObjList jsonb NULL;
--changeset pavel:5
ALTER TABLE public.days RENAME COLUMN "tripobjlist" TO "trip_obj_list";
--changeset pavel:6
ALTER TABLE public.days
ALTER COLUMN trip_obj_list TYPE varchar;
--changeset pavel:7
ALTER TABLE public.days
DROP COLUMN sleep;
ALTER TABLE public.days
DROP COLUMN food;
ALTER TABLE public.days
DROP COLUMN visit;
ALTER TABLE public.days
DROP COLUMN transport;
--changeset pavel:8
ALTER TABLE public.trips
DROP COLUMN curr_ex;